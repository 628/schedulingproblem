package me.sandro.sim.algorithms

import me.sandro.sim.Algorithm
import me.sandro.sim.Person

object DownTheListAlgorithm : Algorithm() {
    override fun run(people: List<Person>) {
        do {
            people.forEach { it.associations.clear() }

            for (i in 0 until 15) {
                var conflict = false
                people.forEach {
                    if (it.associations[i] != null) return@forEach

                    val checked = mutableListOf<Person>()
                    var partner = people.random()

                    var invalid = false
                    while (partner.uuid != it.uuid &&
                        partner.associations[i] != null ||
                        it.associations.values.any { a -> a.uuid == partner.uuid }
                    ) {
                        checked.add(partner)

                        if (checked.containsAll(people)) {
                            invalid = true; break
                        }

                        partner = people.random()
                    }

                    if (invalid) {
                        conflict = true
                        return@forEach
                    }

                    it.associations[i] = partner
                    partner.associations[i] = it
                }

                if (conflict) break
            }
        } while (!validate(people))
    }
}