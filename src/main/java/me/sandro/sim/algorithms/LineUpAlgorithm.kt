package me.sandro.sim.algorithms

import me.sandro.sim.Algorithm
import me.sandro.sim.Person
import java.util.*

object LineUpAlgorithm : Algorithm() {
    override fun run(people: List<Person>) {
        val line = people.shuffled().toMutableList()

        for (i in 0 until 15) {
            line.chunked(2).forEach {
                it[0].associations[i] = it[1]
                it[1].associations[i] = it[0]
            }

            Collections.rotate(line, 1)
        }

        if (!validate(line)) throw Exception("LineUpAlgorithm could not be verified!")
    }
}