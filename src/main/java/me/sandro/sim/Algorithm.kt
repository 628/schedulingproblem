package me.sandro.sim

abstract class Algorithm {
    abstract fun run(people: List<Person>)

    fun validate(people: List<Person>): Boolean {
        for (i in 0 until 15) {
            people.forEach { person ->
                val partner = person.associations[i] ?: return false
                if (people.any { it.uuid != person.uuid && it.associations[i]?.uuid == partner.uuid }) {
                    println("More than one person has $partner as their $i appointment!")
                    return false
                }
            }
        }

        return true
    }
}