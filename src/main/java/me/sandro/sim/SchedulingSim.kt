package me.sandro.sim

import me.sandro.sim.algorithms.DownTheListAlgorithm
import me.sandro.sim.algorithms.LineUpAlgorithm
import java.util.*

data class Person(
    val uuid: UUID,
    val name: String,
    val associations: MutableMap<Int, Person> = mutableMapOf()
) {
    override fun toString(): String {
        return name
    }
}

fun main() {
    SchedulingSim().runSimulation()
}

class SchedulingSim {
    fun runSimulation() {
        println("Beginning simulation...")

        val results = mutableListOf<Long>()
        for (i in 0 until 10) {
            val startTime = System.currentTimeMillis()
            LineUpAlgorithm.run(generateRandomPeople(20))
            val endTime = System.currentTimeMillis()
            results.add(endTime - startTime)
        }

        val lineUpAverage = results.average()

        results.clear()
        for (i in 0 until 10) {
            val startTime = System.currentTimeMillis()
            DownTheListAlgorithm.run(generateRandomPeople(20))
            val endTime = System.currentTimeMillis()
            results.add(endTime - startTime)
        }

        println("Simulation complete! Average run times are shown below.\n")
        println("Line up algorithm took: ${lineUpAverage}ms\nDown the list took: ${results.average()}ms")
    }

    private fun generateRandomPeople(amt: Int): List<Person> {
        val people = mutableListOf<Person>()
        for (i in 0 until amt) {
            val name = javaClass.classLoader.getResource("names.txt")!!
                .readText().split("\n")[Random().nextInt(4944)]

            people.add(Person(UUID.randomUUID(), name))
        }

        return people
    }
}