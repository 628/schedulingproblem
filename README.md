# Scheduling Problem Simulation
This project simulates a scheduling problem computationally, aiming to find the theoretical most efficient method.
## Description
20 people have 15 time slots each to fill with appointments; participants may only make a single appointment with another
participant; scheduling is only complete when **everyone** has **all** of their appointments filled.
## Algorithms
The following algorithms have been tested:
- "Down the list"
- "Line up"

In addition to these, I attempted to implement a "chaos" algorithm where matching was completely random, but after
10 minutes of runtime with a fairly beefy CPU I decided that it was simply too inefficient to properly simulate.

### Down the list
With this method, all participants fill out the time slots in order without moving on until everyone has a pairing; if
a conflict arises at any point the participants will completely start over.  

I attempted to implement a conflict resolution system, but no matter what I tried it was simply more efficient to start over.
### Line up
With this method, the participants line up and initial pairings are formed starting at the beginning of the line; once the first time slot
is filled the line will "rotate" with the person at the end coming to the front, and new parings will be generated for the next time slot.
This process repeats until all slots are filled.
## Results
"Line up" is nearly **24x** faster than "down the list"  

```
Line up algorithm took: 13.4ms
Down the list took: 319.4ms
```

I plan to implement more algorithms as I come up with them and will add the results.